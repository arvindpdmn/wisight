# API Overview #
* This is a brief documentation of the *WiSight* API. In the following documentation, the domain name is omitted for brevity. For example, if *WiSight* is hosted at `http://wisight.site.com`, the API must be called as `http://wisight.site.com/api.php`
* All API calls must contain the network identity.
* All responses are in JSON format. Typical responses are given below.
* Symbol <> indicates a variable. Symbol {} indicates an optional item. Symbol () indicates a choice with choices separated by the | symbol. Symbol [] indicates a POST array.
* Authorization is done using JSON Web Tokens (JWT), which can be generated from the web user interface by a logged in user.

# Summary #
## GET ##
**Network Information** `/api.php?nw=<nwid>`

**Node Information** `/api.php?nw=<nwid>&node=(<nid>|whitelist|all)`

**Sensor Information** `/api.php?nw=<nwid>{&node=(<nid>|whitelist|all)}&sensor=(<sid>|all)`

**Sensor Data** `/api.php?nw=<nwid>&data=1{&node=(<nid>|whitelist|all)}{&sensor=(<sid>|all)}{&start=<sts>}{&end=<ets>}{&limit=<n>}`

**Actions** `/api.php?nw=<nwid>&actions=1`

## POST ##
**Network Identity** `/api.php?nw=<nwid> + panId`

**Network Information** `/api.php?nw=<nwid> + (nodeCount|band|channel|modulation|radioBaudRate|coordTxPower|nodeAssocState)`

**Coordinator Information** `/api.php?nw=<nwid> + name&macAddr&macShortAddr&state`

**Node Information** `/api.php?nw=<nwid> + name&location&macAddr&macShortAddr&macCapability&pushPeriod&state`

**Link Quality** `/api.php?nw=<nwid> + macAddr&lastHopRssi&lastHopCorr`

**Node Build** `/api.php?nw=<nwid> + macAddr&(buildDate|buildTime)`

**Event Information** `/api.php?nw=<nwid> + macAddr&severity&type&msg`

**Sensor Information** `/api.php?nw=<nwid> + macAddr&sensorId&manufacturer&partNum&activeTime&activePwr&standbyPwr&opMode&pushPeriod&alarmPeriod&measure&mode&type&unit&scale&minVal&maxVal&thresholdLow&thresholdHigh&state`

**Sensor Data** `/api.php?nw=<nwid> + macAddr{&lastHopRssi&lastHopCorr}&sensorId[]&data[]`

**Route Information** `/api.php?nw=<nwid> + action=updateRoute&macAddr`

**Rule Timestamp** `/api.php?nw=<nwid> + ruleUid`

# GET #
## Network Information ##
```
Syntax	:	/api.php?nw=<nwid>
			<nwid>	: LPWMN ID	: 2 bytes in hexadecimal

Example	:	/api.php?nw=7455

Response:
{
	"panId":"7455",
	"nodeCount":"1",
	"band":"865-867 MHz",
	"channel":"865399780",
	"modulation":"GFSK",
	"radioBaudRate":"10000",
	"coordMacAddr":"FCC23D000000E8A9",
	"coordTxPower":"1080",
	"nodeAssocState":"Enabled"
}
```

## Node Information ##
```
Syntax	:	/api.php?nw=<nwid>&node=(<nid>|whitelist|all)
			<nwid>	: LPWMN ID	: 2 bytes in hexadecimal
			<nid>	: node ID	: macShortAddr that is 2 bytes in hexadecimal OR
								  macAddr that is 8 bytes in hexadecimal OR
								  "whitelist" that implies whitelisted nodes OR
								  "all" that implies all nodes

Examples:	/api.php?nw=7455&node=0002
			/api.php?nw=7455&node=FCC23D000000E181
			/api.php?nw=7455&node=all

Response:
[
	{
		"nodeUid":"2",
		"name":"E181",
		"location":"Unknown",
		"macAddr":"FCC23D000000E181",
		"macShortAddr":"0002",
		"panId":"7455",
		"lastHopRssi":"-66",
		"lastHopCorr":"45",
		"pushPeriod":"3",
		"macCapability":"80",
		"powerSrc":"Unknown",
		"batteryMAh":"0",
		"state":"Null",
		"radioPartNum":"-",
		"buildDate":"0000-00-00",
		"buildTime":"00:00:00",
		"modifyTs":"2015-07-10 22:14:33"
	},
	{
		"nodeUid":"4",
		"name":"DD0C",
		"location":"Unknown",
		"macAddr":"FCC23D000000DD0C",
		"macShortAddr":"0003",
		"panId":"7455",
		"lastHopRssi":"-68",
		"lastHopCorr":"45",
		"pushPeriod":"3",
		"macCapability":"80",
		"powerSrc":"Unknown",
		"batteryMAh":"0",
		"state":"Active",
		"radioPartNum":"-",
		"buildDate":"0000-00-00",
		"buildTime":"00:00:00",
		"modifyTs":"2015-07-21 21:45:28"
	}
]
```

## Sensor Information ##
```
Syntax	:	/api.php?nw=<nwid>{&node=(<nid>|whitelist|all)}&sensor=(<sid>|all)
			<nwid>	: LPWMN ID	: 2 bytes in hexadecimal
			<nid>	: node ID	: macShortAddr that is 2 bytes in hexadecimal OR
								  macAddr that is 8 bytes in hexadecimal OR
								  "whitelist" that implies whitelisted nodes OR
								  "all" that implies all nodes
			<sid>	: sensor ID	: sensorUid that is a positive integer OR
								  "all" that implies all sensors

Examples:	/api.php?nw=7455&sensor=all
			/api.php?nw=7455&node=FCC23D000000DD0C&sensor=all
			/api.php?nw=7455&sensor=4

Response:
{
	"macAddr":"FCC23D000000DD0C",
	"macShortAddr":"0002",
	"sensorUid":"4",
	"nodeUid":"4",
	"sensorId":"120",
	"manufacturer":"TI",
	"partNum":"MSP430",
	"activeTime":"125 us",
	"activePwr":"-",
	"standbyPwr":"-",
	"opMode":"1",
	"pushPeriod":"3 sec",
	"alarmPeriod":"-",
	"mode":"Digital",
	"type":"Voltage",
	"unit":"Volt",
	"scale":"Milli",
	"minVal":"0",
	"maxVal":"0",
	"thresholdLow":"0",
	"thresholdHigh":"0",
	"lastUpdate":"2015-07-21 21:45:28",
	"nextUpdate":"0000-00-00 00:00:00",
	"state":"Active"
}
```

## Sensor Data ##
```
Syntax	:	/api.php?nw=<nwid>&data=1{&node=(<nid>|whitelist|all)}{&sensor=(<sid>|all)}{&start=<sts>}{&end=<ets>}{&limit=<n>}
			<nwid>	: LPWMN ID	: 2 bytes in hexadecimal
			<nid>	: node ID	: macShortAddr that is 2 bytes in hexadecimal OR
								  macAddr that is 8 bytes in hexadecimal OR
								  "whitelist" that implies whitelisted nodes OR
								  "all" that implies all nodes
			<sid>	: sensor ID	: sensorUid that is a positive integer OR
								  "all" that implies all sensors
			<sts>	: start TS	: timestamp must be of the form YYYY-MM-DD HH:mm:SS in current timezone.
								  Data matching >= <sts> is retrieved.
								  If <sts> is omitted then all data from the oldest is considered.
			<ets>	: end TS	: timestamp must be of the form YYYY-MM-DD HH:mm:SS in current timezone.
								  Data matching < <ets> is retrieved.
								  If <sts> is included but <ets> is not, then current TS is assumed.
			<n>		: number	: a positive integer that defaults to 1000. Maximum is also 1000.
								  Number of latest records to retrieve.
								  
Examples:	/api.php?nw=7455&data=1
			/api.php?nw=7455&data=1&start=2014-05-28 00:00:00&limit=40
			/api.php?nw=7455&data=1&sensor=4&limit=40
			/api.php?nw=7455&data=1&node=FCC23D000000DD0C

Response:
[
	{
		"sensorUid":"5",
		"ts":"2015-08-04 17:47:28",
		"data":"2800"
	},
	{
		"sensorUid":"4",
		"ts":"2015-07-21 21:45:28",
		"data":"2612"
	}
]
```

## Actions ##
```
Syntax	:	/api.php?nw=<nwid>&actions=1
			<nwid>	: LPWMN ID	: 2 bytes in hexadecimal

Example	:	/api.php?nw=7455&actions=1

Response:
[
  {
    "ts": "2016-05-30 09:25:17",
    "type": "cfg-dpi",
    "data": "0241"
  },
  {
    "ts": "2016-05-30 09:25:47",
    "type": "coord-reset",
    "data": ""
  },
  {
    "ts": "2016-05-30 09:26:29",
    "type": "nj-dis",
    "data": ""
  },
  {
    "ts": "2016-05-30 09:26:33",
    "type": "nj-ena",
    "data": ""
  },
  {
    "ts": "2016-05-30 09:26:54",
    "type": "wl-add",
    "data": "FCC23D000000DD0C,FCC23D000000DD0B,FCC23D000000DD0D"
  },
  {
    "ts": "2016-05-30 09:27:00",
    "type": "wl-del",
    "data": "FCC23D000000DD0D"
  },
  {
    "ts": "2016-05-30 09:27:39",
    "type": "dioc",
    "data": "0241,2,2,0"
  }
]
```
