<?php

defined( '_JEXEC' ) or die( 'Restricted access' );

require_once(JPATH_LIBRARIES.'/vendor/autoload.php');
require_once(JPATH_LIBRARIES.'/vendor/firebase/php-jwt/src/JWT.php');

require_once(__DIR__.'/../utils.php');

use Firebase\JWT\JWT;

class Tokener
{
    function __construct() {
        $this->jwt = null;
        $this->data = null;
    }

    function __destruct() {
    }

    private function secretKey()
    {
        return base64_decode(file_get_contents(__DIR__.'/secret.key'));
    }

    public function createToken()
    {
        $user = JFactory::getUser();
        if ($user->guest) return '';

        $now = time();
        $panIds = getUserPanIds();
        $permissions = editPermitted() ? 'rw' : 'r';
        $permissions = trim(str_repeat("$permissions ", count($panIds))); // same permissions for all networks
        $this->data = [
            'iat' => $now,
            'jti' => base64_encode(mcrypt_create_iv(32)),
            'iss' => 'wisense-wisight',
            'nbf' => $now,
            'exp' => $now + 60*60*24*365*3, //  about 3-year validity
            'data'=> [
                'userid' => $user->id,
                'nwks' => implode(' ', $panIds),
                'perms' => $permissions
            ]
        ];

        $secretKey = $this->secretKey();
        $this->jwt = JWT::encode($this->data, $secretKey, 'HS512');
        
        return array('jwt'=>$this->jwt, 'data'=>$this->data);
    }

    public static function checkTokenFields($tkn)
    {
        $now = time();
        $nowstr = date('d-M-Y H:i:s', $now);
        
        $errors = array();

        if ($tkn->nbf > $now) {
            $errors[] = "Cannot use this token before ".date('d-M-Y H:i:s', $tkn->nbf).". Current time is $nowstr.";
        }

        if ($tkn->exp < $now) {
            $errors[] = "Token expired on ".date('d-M-Y H:i:s', $tkn->exp).". Current time is $nowstr.";
        }

        if ($tkn->iss != 'wisense-wisight') {
            # One way to invalidate already issued tokens
            $errors[] = "Token issuer has changed. Please get a new token.";
        }

        if (is_numeric($tkn->data->userid)) {
            $userid = getTableData("#__users", "id", "id=".$tkn->data->userid." AND block=0", 0);
            if ($userid != $tkn->data->userid) {
                $errors[] = "Token is deactivated because the original user account is disabled or deleted.";
            }

            if (isset($_REQUEST['nw']) && is_numeric($_REQUEST['nw'])) {
                $currNw = $_REQUEST['nw'];
                $panIds = getUserPanIds($tkn->data->userid);
                $tknPanIds = explode(' ', trim($tkn->data->nwks));
                if (!in_array($currNw, $tknPanIds)) {
                    $errors[] = "Token does not permit access to the specified network $currNw. Permitted networks in token are ".implode(', ',$tknPanIds).".";
                }
                else if (in_array($currNw, array_diff($tknPanIds, $panIds))) {
                    $errors[] = "Access to the specified network $currNw has been restricted since token was issued. Permitted networks are ".implode(', ',$panIds).".";
                }
                else {
                    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                        $permissions = explode(' ', trim($tkn->data->perms));
                        $idx = array_search($currNw, $tknPanIds);
                        if ($idx >= count($permissions)) {
                            # Assume read-only
                            $errors[] = "You are not permitted to modify the specified network $currNw.";
                        }
                        else if ($permissions[$idx]!='rw') {
                            $errors[] = "You don't have write permissions to the specified network $currNw.";                        
                        }
                    }
                }
            }
            else {
                $errors[] = "API call does not have a valid network ID.";        
            }
        }
        else {
            $errors[] = "Token contains an invalid user ID.";
        }
        
        return $errors;
    }

    public function checkToken()
    {
        $errors = array();
        
        $headers = getallheaders();
        if (isset($headers['Authorization'])) {
            list($jwt) = sscanf($headers['Authorization'], 'Token %s');
            if ($jwt) {
                try {
                    $secretKey = $this->secretKey();
                    $token = JWT::decode($jwt, $secretKey, array('HS512'));
                    $errors = $this->checkTokenFields($token);
                }
                catch (Exception $e) {
                    header('HTTP/1.0 401 Unauthorized'); // 'HTTP/1.0 405 Method Not Allowed'
                    $errors[] = "Token validation failed.";
                }
            }
            else {
                header('HTTP/1.0 400 Bad Request');
                $errors[] = "An empty token was received.";
            }
        }
        else {
            header('HTTP/1.0 400 Bad Request');
            $errors[] = "No token was received.";
        }
        
        return $errors;
    }

    public function showToken()
    {
        if ($this->jwt===null || $this->data===null) {
            echo "<div class='box-warning'>A valid token was not found or could not be created.</div>";
            return;
        }
    ?>
    <h2>Your New Token</h2>
    <table class="tokenTable">
        <tbody>
            <tr>
                <th>Token</th>
                <td class="wordwrap box-info token"><?php echo $this->jwt; ?></td>
            </tr>
            <tr>
                <th>Issuer</th>
                <td><?php echo $this->data['iss']; ?></td>
            </tr>
            <tr>
                <th>Issued On</th>
                <td><?php echo date('d-M-Y H:i:s', $this->data['iat']); ?></td>
            </tr>
            <tr>
                <th>Valid From</th>
                <td><?php echo date('d-M-Y H:i:s', $this->data['nbf']); ?></td>
            </tr>
            <tr>
                <th>Expires On</th>
                <td><?php echo date('d-M-Y H:i:s', $this->data['exp']); ?></td>
            </tr>
            <tr>
                <th>Allowed Networks</th>
                <td><?php echo $this->data['data']['nwks'] . " (" . $this->data['data']['perms'] . ")"; ?></td>
            </tr>
        </tbody>
    </table>
    <?php
    }
}
