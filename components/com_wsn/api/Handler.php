<?php

defined( '_JEXEC' ) or die( 'Restricted access' );

require_once JPATH_BASE.'/components/com_wsn/dbaccess.php';

class Handler
{
    function __construct() {
        $this->p = null; //  contains the GET/POST params
    }

    function __destruct() {
    }

    public function isValidApiCall()
    {
        if (!isset($this->p['nw'])) return false;
        return true;
    }

    public function sendJsonResponse($rspData)
    {
        header('Content-Disposition: attachment; charset=UTF-8; filename="data.json"');
        header('Content-Type: application/json');
        header('Keep-Alive: timeout=120, max=1000'); // Web server settings will have precedence
        echo json_encode($rspData);
    }

	public function getDevName($devId)
	{
		switch ($devId)
		{
			case 0x1: return "AT24C01C_1";
			case 0x2: return "AT24MAC602_1";
			case 0x4: return "M24M01_1";
			case 0x9: return "LM75B_1";
			case 0xa: return "TMP102_1";
			case 0xb: return "NTC_THERMISTOR_1";
			case 0xc: return "NTC_THERMISTOR_2";
			case 0x11: return "SFH_7773_1";
			case 0x12: return "TSL45315_1";
			case 0x13: return "MPU6050_1";
			case 0x14: return "ADXL345_1";
			case 0x15: return "SHT10_1";
			case 0x16: return "LIS3MDLTR_1";
			case 0x19: return "CC2520_1";
			case 0x1a: return "CC1200_1";
			case 0x1b: return "CC1101_1";
			case 0x1c: return "CC3100_1";
			case 0x20: return "MPL115A1_1";
			case 0x28: return "MAX_SONAR_1";
			case 0x30: return "EKMC160111X_1";
			case 0x40: return "LED_1";
			case 0x41: return "LED_2";
			case 0x50: return "REED_SWITCH_1";
			case 0x51: return "SPST_SWITCH_1";
			case 0x60: return "MP3V5050GP_1";
			case 0x61: return "HE055T01_1";
			case 0x62: return "MP3V5004GP_1";
			case 0x65: return "LLS_1";
			case 0x68: return "BATT_1";
			case 0x70: return "AD7797_1";
			case 0x78: return "ON_CHIP_VCC_SENSOR";
			case 0x79: return "ON_CHIP_TEMP_SENSOR";
			case 0x80: return "SYNC_RT_1";
			case 0xb0: return "CC2D33S_1";
			case 0x90: return "GPIO_REMOTE_CTRL";
			case 0x91: return "CHIRP_PWLA_1";
			case 0x92: return "FC_28_1";
			case 0x93: return "WSMS100_1";
			case 0xb8: return "MPL3115A2_1";
			case 0xb9: return "MPL115A2_1";
			case 0xba: return "BMP180_1";
			case 0xc0: return "MMA7660FC_1";
			case 0xe0: return "I2C_SW_BUS_1";
			case 0xe1: return "I2C_SW_BUS_2";
			case 0xe8: return "SPI_HW_BUS_1";
			case 0xfe: return "32KHZ_CRYSTAL";
			case 0xff: return "GENERIC";
			case 0xa0: return "UART_HW_1";
			default: return "UNKNOWN";
		}
	}
}
