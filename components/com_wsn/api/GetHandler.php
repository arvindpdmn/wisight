<?php

defined( '_JEXEC' ) or die( 'Restricted access' );

require_once(__DIR__.'/Handler.php');

class GetHandler extends Handler
{
    public function execute() {
        $uri = JFactory::getURI();
        parse_str($uri->getQuery(), $this->p);

        if (!$this->isValidApiCall()) exit();

        $rspData = null;

        if (count($this->p)==1) {
            $rspData = $this->showNwInfo();
        }
        elseif (count($this->p)==2 && isset($this->p['node'])) {
            $rspData = $this->showNodeInfo();
        }
        elseif (count($this->p)==2 && isset($this->p['sensor']) ||
            count($this->p)==3 && isset($this->p['node']) && isset($this->p['sensor'])) {
            $rspData = $this->showSensorInfo();
        }
        elseif (count($this->p)>=2 &&
            isset($this->p['data']) && $this->p['data']=='1') {
            $rspData = $this->showSensorData();
        }
        elseif (count($this->p)==2 && 
            isset($this->p['actions']) && $this->p['actions']=='1') {
            $rspData = $this->showActions();
        }

        if ($rspData)
            $this->sendJsonResponse($rspData);
    }

    public function isDateValid($input) {
        return true;
    }

    public function showNwInfo() {
        $data = executeQuery("SELECT * FROM NetworkInfo WHERE panId='".$this->p['nw']."' LIMIT 1", 4);
        return $data[0];
    }

    public function showNodeInfo() {
        if ($this->p['node']=='whitelist') {
            $whiteList = executeQuery("SELECT macAddr FROM NodeWhiteList WHERE panId='".$this->p['nw']."'", 3);
            if ($whiteList) {
                $data = executeQuery("SELECT NodeWhiteList.macAddr AS whiteMacAddr, NodeInfo.* FROM NodeWhiteList LEFT JOIN NodeInfo ON NodeWhiteList.macAddr=NodeInfo.macAddr WHERE NodeWhiteList.panId='".$this->p['nw']."'", 4);
                return $data;
            }
            else return null;
        }
        else if ($this->p['node']=='all') $filter = " AND macShortAddr!='0001'"; // don't show coordinator node
        else $filter = " AND (macShortAddr='".strtoupper($this->p['node'])."' OR macAddr='".strtoupper($this->p['node'])."')";

        $data = executeQuery("SELECT * FROM NodeInfo WHERE panId='".$this->p['nw']."' AND state!='Null' $filter", 4);

        if ($this->p['node']=='all') return $data;
        else return $data[0];
    }

    public function showSensorInfo() {
        if ($this->p['sensor']=='all') $filter = "";
        else $filter = " AND sensorUid='".$this->p['sensor']."'";

        if (isset($this->p['node']) && $this->p['node']=='whitelist') {
            $whiteList = executeQuery("SELECT macAddr FROM NodeWhiteList WHERE panId='".$this->p['nw']."'", 3);
            if ($whiteList) $filter .= " AND macAddr IN ('".implode("', '", $whiteList)."')";
        }
        else if (isset($this->p['node']) && $this->p['node']!='all') {
            $filter .= " AND (macShortAddr='".strtoupper($this->p['node'])."' OR macAddr='".strtoupper($this->p['node'])."')";
        }

        $data = executeQuery("SELECT macAddr, macShortAddr, SensorInfo.* FROM SensorInfo, NodeInfo WHERE panId='".$this->p['nw']."' AND NodeInfo.nodeUid=SensorInfo.nodeUid $filter", 4);

        // Include rules of each sensor
        $sensorUids = array();
        foreach ($data as $d) $sensorUids[] = $d['sensorUid'];
        $rules = executeQuery("SELECT sensorUid, ruleUid, type, threshold, actionOn, notifyPeriod, notifiedTs, mobileNum, UNIX_TIMESTAMP(NOW())-UNIX_TIMESTAMP(notifiedTs) AS elapsedSecs FROM Rule WHERE sensorUid IN ('".implode("', '",$sensorUids)."')", 4);
        foreach ($rules as $r) {
            foreach ($data as &$d) {
                if ($r['sensorUid']==$d['sensorUid']) {
                    if (!isset($d['rules'])) $d['rules'] = array();
                    unset($r['sensorUid']);
                    $d['rules'][] = $r;
                    break;
                }
            }
        }

        if ($this->p['sensor']=='all') return $data;
        else return $data[0];
    }

    public function showSensorData() {
        $filter = "";

        if (isset($this->p['node']) && $this->p['node']=='whitelist') {
            $whiteList = executeQuery("SELECT macAddr FROM NodeWhiteList WHERE panId='".$this->p['nw']."'", 3);
            if ($whiteList) $filter .= " AND macAddr IN ('".implode("', '", $whiteList)."')";
        }
        else if (isset($this->p['node']) && $this->p['node']!='all') {
            $filter .= " AND (macShortAddr='".strtoupper($this->p['node'])."' OR macAddr='".strtoupper($this->p['node'])."')";
        }

        if (isset($this->p['sensor']) && $this->p['sensor']!='all')
            $filter .= " AND sensorUid='".$this->p['sensor']."'";

        if (isset($this->p['limit'])) $limit = min(array($this->p['limit'],1000));
        else $limit = 1000;

        if (isset($this->p['start']) && $this->isDateValid($this->p['start'])) $start = $this->p['start'];
        else $start = 1970; # an arbitrary start point

        if (isset($this->p['end']) && $this->isDateValid($this->p['end'])) $end = $this->p['end'];
        else $end = 'NOW()';

        $data = executeQuery("SELECT SensorData.* FROM SensorData, SensorInfo, NodeInfo ".
                    " WHERE panId='".$this->p['nw']."' ".
                    " AND SensorData.sensorUid=SensorInfo.sensorUid ".
                    " AND NodeInfo.nodeUid=SensorInfo.nodeUid $filter ".
                    " AND ts>='$start' AND ts<'$end' ".
                    " ORDER BY ts DESC ".
                    " LIMIT $limit", 4);

        return $data;
    }

    public function showActions() {
        // Check if anything needs to be pushed to the client in response
        $actions = executeQuery("SELECT * FROM PendingAction ORDER BY ts ASC", 4);
        executeQuery("DELETE FROM PendingAction ORDER BY ts ASC LIMIT ".count($actions));
        if ($actions) return $actions;
        else return NULL;
    }
}
