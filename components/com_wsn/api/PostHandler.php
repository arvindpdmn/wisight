<?php

defined( '_JEXEC' ) or die( 'Restricted access' );

require_once(__DIR__.'/Handler.php');

class PostHandler extends Handler
{
    public function execute() {
        $uri = JFactory::getURI();
        parse_str($uri->getQuery(), $this->p);
        $this->p = array_merge($this->p, $_POST);

        // echo "PostHandler: $uri: "; print_r($this->p);

        if (!$this->isValidApiCall()) exit();

        if (count($this->p)==2 && isset($this->p['panId']) &&
            $this->p['nw']==$this->p['panId']) {
            $this->updatePanId();
        }
        else if (count($this->p)==2 &&
                    (isset($this->p['nodeCount']) || isset($this->p['band']) ||
                     isset($this->p['channel']) || isset($this->p['modulation']) ||
                     isset($this->p['radioBaudRate']) || isset($this->p['coordTxPower']) ||
                     isset($this->p['nodeAssocState']))
                ) {
            $field = array_pop(array_diff(array_keys($this->p), array('nw')));
            $this->updateNetworkInfoField($field);
        }
        else if (count($this->p)==5 &&
            isset($this->p['name']) && isset($this->p['macAddr']) &&
            isset($this->p['macShortAddr']) && isset($this->p['state'])) {
            $this->updateCoordExtAddr();
        }
        else if (count($this->p)==8 &&
            isset($this->p['name']) && isset($this->p['location']) &&
            isset($this->p['macAddr']) && isset($this->p['macShortAddr']) &&
            isset($this->p['macCapability']) && isset($this->p['pushPeriod']) &&
            isset($this->p['state'])) {
            $this->updateNodeInfo();
        }
        else if (count($this->p)==4 &&
            isset($this->p['macAddr']) &&
            isset($this->p['lastHopRssi']) && isset($this->p['lastHopCorr'])) {
            $this->updateRssiAndCorrelation();
        }
        else if (count($this->p)==3 &&
            isset($this->p['macAddr']) && isset($this->p['buildDate'])) {
            $this->updateNodeBuildDate();
        }
        else if (count($this->p)==3 &&
            isset($this->p['macAddr']) && isset($this->p['buildTime'])) {
            $this->updateNodeBuildTime();
        }
        else if (count($this->p)==5 &&
            isset($this->p['macAddr']) && isset($this->p['severity']) &&
            isset($this->p['type']) && isset($this->p['msg'])) {
            $this->insertEvent();
        }
        else if (count($this->p)==21 &&
            isset($this->p['macAddr']) && isset($this->p['sensorId']) &&
            isset($this->p['manufacturer']) && isset($this->p['partNum']) &&
            isset($this->p['activeTime']) && isset($this->p['activePwr']) &&
            isset($this->p['standbyPwr']) && isset($this->p['opMode']) &&
            isset($this->p['pushPeriod']) && isset($this->p['alarmPeriod']) &&
            isset($this->p['measure']) && isset($this->p['mode']) &&
            isset($this->p['type']) && isset($this->p['unit']) &&
            isset($this->p['scale']) && isset($this->p['minVal']) &&
            isset($this->p['maxVal']) && isset($this->p['thresholdLow']) &&
            isset($this->p['thresholdHigh']) && isset($this->p['state'])) {
            // API is open but we don't expect this call to be made since info is hard coded
            $this->updateSensorInfo();
        }
        else if (count($this->p)>=4 &&
            isset($this->p['macAddr']) && isset($this->p['sensorId']) &&
            isset($this->p['data'])) {
            if (isset($this->p['lastHopRssi']) && isset($this->p['lastHopCorr'])) {
                $this->updateRssiAndCorrelation();
            }
            $this->updateSensorData();
        }
        else if (count($this->p)==3 &&
            isset($this->p['action']) && $this->p['action']=="updateRoute" &&
            isset($this->p['macAddr'])) {
            $this->updateRouteInfo();
        }
        else if (count($this->p)==2 &&
            isset($this->p['ruleUid'])) {
            $this->updateRuleNotifiedTs();
        }
    }

    public function updatePanId() {
        $panPresent = executeQuery("SELECT COUNT(*) FROM NetworkInfo WHERE panId='".$this->p['nw']."'", 0);
        if (!$panPresent) {
            executeQuery("INSERT INTO NetworkInfo (panId) VALUES('".$this->p['nw']."')");
        }
    }

    public function updateNetworkInfoField($field) {
        executeQuery("UPDATE NetworkInfo SET $field='".$this->p[$field]."' WHERE panId='".$this->p['nw']."'");
    }

    public function updateCoordExtAddr() {
        $nodePresent = executeQuery("SELECT COUNT(*) FROM NodeInfo WHERE macAddr='".$this->p['macAddr']."'", 0);
        if (!$nodePresent) {
            executeQuery("INSERT INTO NodeInfo (name, macAddr, macShortAddr, panId, state) ".
                "VALUES('LPWMN Coord','".$this->p['macAddr']."','".$this->p['macShortAddr']."','".$this->p['nw']."','Active')");
        }
        executeQuery("UPDATE NetworkInfo SET coordMacAddr='".$this->p['macAddr']."' WHERE panId='".$this->p['nw']."'");
    }

    public function updateNodeInfo() {
        // Change the state of other nodes having the same short address
        executeQuery("UPDATE NodeInfo SET state='NULL' WHERE panId='".$this->p['nw']."' AND macShortAddr='".$this->p['macShortAddr']."' AND state!='NULL' AND macAddr!='".$this->p['macAddr']."'");

        // TODO Update correctly number of sensors and other node information
        $nodePresent = executeQuery("SELECT COUNT(*) FROM NodeInfo WHERE macAddr='".$this->p['macAddr']."' LIMIT 1", 0);
        if ($nodePresent) {
            executeQuery("UPDATE NodeInfo SET macShortAddr='".$this->p['macShortAddr']."', panId='".$this->p['nw']."', macCapability='".$this->p['macCapability']."', state='Associated', modifyTs=NOW() WHERE macAddr='".$this->p['macAddr']."'");
        }
        else {
            // Give default name and location at insert
            executeQuery("INSERT INTO NodeInfo (name, location, macAddr, macShortAddr, panId, macCapability, pushPeriod, state) ".
                "VALUES('".$this->p['name']."', '".$this->p['location']."', '".$this->p['macAddr']."', '".$this->p['macShortAddr']."', '".
                $this->p['nw']."', '".$this->p['macCapability']."', ".$this->p['pushPeriod'].", '".$this->p['state']."')");
            executeQuery("INSERT INTO EventInfo (nodeUid, ts, severity, type, msg) ".
                "VALUES((SELECT nodeUid from NodeInfo WHERE panId='".$this->p['nw']."' AND macAddr='".$this->p['macAddr']."' AND state!='NULL' ORDER BY modifyTs DESC LIMIT 1), NOW(), 'Info', 'NodeCommissioning', 'Node identified by its MAC address has been auto-commissioned.')");
        }
    }

    public function updateNodeBuildDate() {
        executeQuery("UPDATE NodeInfo SET buildDate='".$this->p['buildDate']."' WHERE panId='".$this->p['nw']."' AND macAddr='".$this->p['macAddr']."'");
    }

    public function updateNodeBuildTime() {
        executeQuery("UPDATE NodeInfo SET buildTime='".$this->p['buildTime']."' WHERE panId='".$this->p['nw']."' AND macAddr='".$this->p['macAddr']."'");
    }
    
    public function updateRssiAndCorrelation() {
        executeQuery("UPDATE NodeInfo SET lastHopRssi='".$this->p['lastHopRssi']."', lastHopCorr='".$this->p['lastHopCorr']."', state='Active' WHERE panId='".$this->p['nw']."' AND macAddr='".$this->p['macAddr']."' AND state!='NULL' ORDER BY modifyTs DESC LIMIT 1");
    }

    public function insertEvent() {
        executeQuery("INSERT INTO EventInfo (nodeUid, ts, severity, type, msg) ".
            "VALUES((SELECT nodeUid from NodeInfo WHERE panId='".$this->p['nw']."' AND macAddr='".$this->p['macAddr']."' AND state!='NULL' ORDER BY modifyTs DESC LIMIT 1), ".
            "NOW(), '".$this->p['severity']."', '".$this->p['type']."', '".$this->p['msg']."')");
    }

    public function updateSensorInfo() {
        $sensorPresent = executeQuery("SELECT COUNT(*) FROM SensorInfo WHERE ".
            "nodeUid=(SELECT nodeUid from NodeInfo WHERE panId='".$this->p['nw']."' AND macAddr='".$this->p['macAddr']."' AND state!='NULL' ORDER BY modifyTs DESC LIMIT 1) ".
            "AND sensorId='".$this->p['sensorId']."'", 0);
        if (!$sensorPresent) { // TODO Update existing sensor information
            executeQuery("INSERT INTO SensorInfo (nodeUid, sensorId, manufacturer, partNum, activeTime, activePwr, standbyPwr, ".
                "opMode, pushPeriod, alarmPeriod, measure, mode, type, unit, scale, minVal, maxVal, thresholdLow, thresholdHigh, state) ".
                "VALUES((SELECT nodeUid from NodeInfo WHERE panId='".$this->p['nw']."' AND macAddr='".$this->p['macAddr']."' AND state!='NULL' ORDER BY modifyTs DESC LIMIT 1), ".
                "'".$this->p['sensorId']."', '".$this->p['manufacturer']."', '".$this->p['partNum']."', '".$this->p['activeTime']."', '".$this->p['activePwr']."', '".$this->p['standbyPwr']."', ".
                "'".$this->p['opMode']."', '".$this->p['pushPeriod']."', '".$this->p['alarmPeriod']."', '".$this->p['measure']."', '".$this->p['mode']."', '".$this->p['type']."', '".$this->p['unit']."', '".$this->p['scale']."', ".
                "'".$this->p['minVal']."', '".$this->p['maxVal']."', '".$this->p['thresholdLow']."', '".$this->p['thresholdHigh']."', '".$this->p['state']."')");
        }
    }

    public function updateSensorData() {
        // TODO Implement sensor info by coding gateway to query the nodes: hard coded for now
        $fixedSensors = array(
            0x78 => array("TI", "MSP430", "125 us", "-", "-", 1, "3 sec", "-", "Instantaneous", "Digital", "Voltage", "Volt", "Milli", 0, 0, 0, 0, "Unknown"),
            0x9 => array("NXP", "LM75B", "125 us", "-", "-", 1, "3 sec", "-", "Instantaneous", "Digital", "Temperature", "Celsius", "Centi", 0, 0, 0, 0, "Unknown"),
            0x12 => array("AMS-TAOS", "TSL45315", "400 ms", "-", "-", 1, "3 sec", "-", "Instantaneous", "Digital", "Luminance", "Lux", "Unit", 0, 0, 0, 0, "Unknown"),
            0x91 => array("CERN", "CHIRP_PWLA", "400 ms", "-", "-", 1, "3 sec", "-", "Instantaneous", "Digital", "Moisture", "Number", "Unit", 0, 0, 0, 0, "Unknown"),
            0x60 => array("Freescale", "MP3V5050GP", "400 ms", "-", "-", 1, "3 sec", "-", "Instantaneous", "Analog", "Pressure", "Inch", "Milli", 0, 0, 0, 0, "Unknown"),
            0x62 => array("Freescale", "MP3V5004GP", "400 ms", "-", "-", 1, "3 sec", "-", "Instantaneous", "Analog", "Pressure", "Inch", "Milli", 0, 0, 0, 0, "Unknown"),
            0x93 => array("WiSense", "WSMS100", "400 ms", "-", "-", 1, "3 sec", "-", "Instantaneous", "Digital", "Moisture", "Percent", "Centi", 0, 0, 0, 0, "Unknown"),
            0xb => array("Vishay", "NTCALUG02A", "400 ms", "-", "-", 1, "3 sec", "-", "Instantaneous", "Analog", "Temperature", "Celsius", "Centi", 0, 0, 0, 0, "Unknown")
        );

        $coordMacAddr = executeQuery("SELECT coordMacAddr FROM NetworkInfo WHERE panId='".$this->p['nw']."'", 0);
        if ($coordMacAddr==$this->p['nw']) {
            header('HTTP/1.0 400 Bad Request');
            echo "Sensor data from PAN coordinator is unexpected.";
            return;
        }
        else if (!is_array($this->p['sensorId']) || !is_array($this->p['data']) ||
            count($this->p['sensorId'])!=count($this->p['data'])) {
            header('HTTP/1.0 400 Bad Request');
            echo "Sensor data is not in expected array type.";
            return;
        }
        else {
            for ($i=0; $i<count($this->p['sensorId']); $i++) {
                $sensorId = $this->p['sensorId'][$i];
                $data = $this->p['data'][$i];

                $sensorPresent = executeQuery("SELECT COUNT(*) FROM SensorInfo WHERE ".
                    "nodeUid=(SELECT nodeUid from NodeInfo WHERE panId='".$this->p['nw']."' AND macAddr='".$this->p['macAddr']."' AND state!='NULL' ORDER BY modifyTs DESC LIMIT 1) ".
                    "AND sensorId='$sensorId'", 0);
                if (!$sensorPresent && isset($fixedSensors[$sensorId])) { // TODO Update existing sensor information
                    executeQuery("INSERT INTO SensorInfo (nodeUid, sensorId, manufacturer, partNum, activeTime, activePwr, standbyPwr, ".
                        "opMode, pushPeriod, alarmPeriod, measure, mode, type, unit, scale, minVal, maxVal, thresholdLow, thresholdHigh, state) ".
                        "VALUES((SELECT nodeUid from NodeInfo WHERE panId='".$this->p['nw']."' AND macAddr='".$this->p['macAddr']."' AND state!='NULL' ORDER BY modifyTs DESC LIMIT 1), ".
                        "'$sensorId', '".implode("', '",$fixedSensors[$sensorId])."')");
                }

                $nodeUid = executeQuery("SELECT nodeUid FROM NodeInfo WHERE panId='".$this->p['nw']."' AND macAddr='".$this->p['macAddr']."' AND state!='NULL' ORDER BY modifyTs DESC LIMIT 1", 0);
                executeQuery("UPDATE SensorInfo SET SensorInfo.state='Active', lastUpdate=NOW(), nextUpdate=DATE_ADD(NOW(), INTERVAL (SELECT pushPeriod FROM NodeInfo WHERE nodeUid=$nodeUid) SECOND) ".
                    "WHERE SensorInfo.nodeUid=$nodeUid AND sensorId='$sensorId'");
                executeQuery("INSERT INTO SensorData (sensorUid, ts, data) VALUES(".
                    "(SELECT sensorUid FROM SensorInfo,NodeInfo WHERE SensorInfo.nodeUid=NodeInfo.nodeUid AND panId='".$this->p['nw']."' AND macAddr='".$this->p['macAddr']."' AND NodeInfo.state!='NULL' AND sensorId='$sensorId' ORDER BY modifyTs DESC LIMIT 1), ".
                    "NOW(), '$data')");

                // Check the rules and send notifications if needed
                $rules = getTableData("Rule,NodeInfo,SensorInfo",
                            "Rule.nodeUid, macShortAddr, Rule.sensorUid, sensorId, Rule.type, threshold, notifyPeriod, notifiedTs, mobileNum, ruleUid, UNIX_TIMESTAMP(NOW())-UNIX_TIMESTAMP(notifiedTs) AS elapsedSecs",
                            "Rule.nodeUid=NodeInfo.nodeUid AND Rule.sensorUid=SensorInfo.sensorUid AND actionOn='Cloud' AND macAddr='".$this->p['macAddr']."' AND SensorInfo.sensorId='$sensorId'");
                if ($rules) {
                    foreach ($rules as $r) {
                        $devName = $this->getDevName($r[3]);
                        if ($r[10]>$r[6]) { // notifyPeriod exceeded: record a trigger event
                            if ($r[4]=="LowThreshold" && $data<$r[5]) {
                                $msg = "Cloud; ShortAddr=0x$r[1]; Sensor=$devName; Below low threshold: $data < $r[5].";
                            }
                            else if ($r[4]=="HighThreshold" && $data>$r[5]) {
                                $msg = "Cloud; ShortAddr=0x$r[1]; Sensor=$devName; High threshold exceeded: $data > $r[5].";
                            }
                            else continue;
                            executeQuery("INSERT INTO EventInfo VALUES($r[0], NOW(), 'Warn', 'RuleTrigger', '$msg')");
                            executeQuery("UPDATE Rule SET notifiedTs=NOW() WHERE ruleUid=$r[9]");
                            $this->sendSms($r[8], $msg);
                        }
                    }
                }
            }
        }
    }

    public function updateRouteInfo() {
        $coordUid = executeQuery("SELECT nodeUid from NodeInfo WHERE panId='".$this->p['nw']."' AND macShortAddr='0001' AND state!='NULL' ORDER BY modifyTs DESC LIMIT 1", 0);
        $nodeUid = executeQuery("SELECT nodeUid from NodeInfo WHERE panId='".$this->p['nw']."' AND macAddr='".$this->p['macAddr']."' AND state!='NULL' ORDER BY modifyTs DESC LIMIT 1", 0);
        $routePresent = executeQuery("SELECT COUNT(*) FROM RouteInfo WHERE nodeUid=$nodeUid", 0);
        if ($routePresent) {
            executeQuery("UPDATE RouteInfo SET route='$nodeUid-$coordUid' WHERE nodeUid='$coordUid'");
        }
        else {
            executeQuery("INSERT INTO RouteInfo (nodeUid, numHops, route) VALUES($nodeUid, 1, '$nodeUid-$coordUid')");
        }

        executeQuery("INSERT INTO EventInfo (nodeUid, ts, severity, type, msg) ".
            "VALUES($nodeUid, NOW(), 'Info', 'NodeRouteDiscovery', 'Node route has been received.')");
    }

    public function updateRuleNotifiedTs() {
        executeQuery("UPDATE Rule SET notifiedTs=NOW() WHERE ruleUid='".$this->p['ruleUid']."'");
    }

    public function sendSms($mobile, $msg)
    {
        $smsgw = json_decode(file_get_contents(__DIR__."/smsgateway.json"));
        $url = $smsgw->url.
            "?user=".$smsgw->params->user.
            "&pwd=".$smsgw->params->pwd.
            "&to=".$mobile.
            "&sid=".$smsgw->params->sid.
            "&msg=".urlencode($msg).
            "&gwid=".$smsgw->params->gwid.
            "&fl=".$smsgw->params->fl;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$response = curl_exec($ch);
	}
}
