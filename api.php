<?php

define('_JEXEC', 1);
define('DS', DIRECTORY_SEPARATOR);

if (file_exists(dirname(__FILE__) . '/defines.php')) {
	include_once dirname(__FILE__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
	define('JPATH_BASE', dirname(__FILE__));
	require_once JPATH_BASE.'/includes/defines.php';
}

require_once JPATH_BASE.'/includes/framework.php';

// Mark afterLoad in the profiler.
JDEBUG ? $_PROFILER->mark('afterLoad') : null;

// Instantiate the application.
$app = JFactory::getApplication('site');

// Initialise the application.
$app->initialise();

require_once JPATH_BASE.'/components/com_wsn/dbaccess.php';
require_once JPATH_BASE.'/components/com_wsn/api/Tokener.php';
require_once JPATH_BASE.'/components/com_wsn/api/GetHandler.php';
require_once JPATH_BASE.'/components/com_wsn/api/PostHandler.php';

/* TODO
require_once JPATH_LIBRARIES."/vendor/slim/slim/Slim/Slim.php";
\Slim\Slim::registerAutoloader();

$slim = new \Slim\Slim();
$slim->get('/nw/:nw', function ($nw) {
	echo "Hello, $nw";
});
$slim->run();
*/

$tokener = new Tokener();
$errors = $tokener->checkToken();
if ($errors) {
	echo "Authorization failed:\n";
	foreach ($errors as $err) {
		echo "  $err\n";
	}
	exit();
}

$method = $_SERVER['REQUEST_METHOD'];
switch ($method) {
	case 'GET':
		$handler = new GetHandler();
		$handler->execute();
		break;
	case 'POST':
		$handler = new PostHandler();
		$handler->execute();
		break;
	case 'PUT':
	case 'HEAD':
	case 'DELETE':
	case 'OPTIONS':
		echo "ERR: Request method $method is not implemented.";
		break;
	default:
		echo "ERR: Request method $method is not recognized.";
		break;
}
